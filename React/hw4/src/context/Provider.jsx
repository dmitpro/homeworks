import React, {useReducer} from 'react';

import {initialState} from '../store/initialState';
import {reducer} from '../store/reducer';
import {START_TIMER, STOP_TIMER, RESET_TIMER} from '../store/constants';
import {context} from './context';

export const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  let {interval} = state;
  const value = {
    state,

    startTimer: () => {

      if (!state.isActive) {
        interval = setInterval(() => dispatch({type: START_TIMER, payload: interval}), 10);
      }
    },

    stopTimer: () => {
      clearInterval(interval);
      dispatch({type: STOP_TIMER})
    },

    resetTimer: () => {
      clearInterval(interval);
      dispatch({type: RESET_TIMER})
    }
  }

  return (
    <context.Provider value={value}>
      {children}
    </context.Provider>
  )
}