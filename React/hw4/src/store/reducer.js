import {RESET_TIMER, START_TIMER, STOP_TIMER} from './constants';

export const reducer = (state, {type, payload = null}) => {

  switch (type) {

    case START_TIMER:

      const {tens, seconds} = state;
      let newTens = +tens + 1;
      let newSeconds = seconds;

      if (newTens < 10) {
        newTens = "0" + newTens;
      }

      if (newTens > 99) {
        newTens = "00";
        newSeconds = +newSeconds + 1;

        if (newSeconds < 10) {
          newSeconds = "0" + newSeconds;
        }
      }

      return {...state, tens: newTens, seconds: newSeconds, isActive: true, interval: payload}

    case STOP_TIMER:
      return {...state, isActive: false};

    case RESET_TIMER:
      return {...state, seconds: '00', tens: '00', isActive: false};

    default:
      return state;
  }
}
