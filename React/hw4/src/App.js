import React from 'react';

import './App.scss';

import {Timer} from './client/components/Timer';
import {TimerControl} from './client/components/TimerControl';
import {Provider} from './context/Provider';

function App() {
  return (
    <div className="App">
      <h1>Stopwatch</h1>
      <h2>React Stopwatch</h2>
      <Provider>
        <Timer/>
        <TimerControl/>
      </Provider>
    </div>
  );
}

export default App;
