import React, {useContext} from 'react';

import {context} from '../../../context/context'

export const TimerControl = () => {

  const {startTimer, stopTimer, resetTimer} = useContext(context);

  return (
    <div className="timer-control">
      <button onClick={startTimer} className="button-start">Start</button>
      <button onClick={stopTimer} className="button-stop">Stop</button>
      <button onClick={resetTimer} className="button-reset">Reset</button>
    </div>
  )
}
