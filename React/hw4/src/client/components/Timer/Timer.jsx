import React, {useContext} from 'react'
import {context} from '../../../context/context';

export const Timer = () => {
  const {tens, seconds} = useContext(context).state;

  return (
    <div className="timer">
      <p><span id="seconds">{seconds}</span>:<span id="tens">{tens}</span></p>
    </div>
  )
}


// import React, {useState, useEffect} from 'react';
//
// export const Timer = () => {
//   const [tens, setTens] = useState('00');
//   const [seconds, setSeconds] = useState('00');
//   const [isActive, setIsActive] = useState(false);
//
//   function startTimer() {
//     setIsActive(true);
//   }
//
//   function stopTimer() {
//     setIsActive(false);
//   }
//
//   function resetTimer() {
//     setIsActive(false);
//     setTens('00');
//     setSeconds('00');
//   }
//
//   useEffect(() => {
//     let interval = null;
//     if (isActive) {
//       interval = setInterval(() => {
//         setTens(tens => +tens +1)
//
//         if (tens < 9) {
//           setTens(tens => "0" + tens);
//         }
//
//         if (tens > 98) {
//           setTens("00");
//           setSeconds(seconds => +seconds + 1)
//           if (seconds < 9) {
//             setSeconds(seconds => "0" + seconds);
//           }
//         }
//       }, 10);
//     } else if (!isActive) {
//       clearInterval(interval);
//     }
//     return () => clearInterval(interval);
//   }, [isActive, tens, seconds]);
//
//   return (
//       <div className="timer">
//         <p><span id="seconds">{seconds}</span>:<span id="tens">{tens}</span></p>
//         <div className="timer-control">
//           <button onClick={startTimer} className="button-start">Start</button>
//           <button onClick={stopTimer} className="button-stop">Stop</button>
//           <button onClick={resetTimer} className="button-reset">Reset</button>
//         </div>
//       </div>
//   )
// }