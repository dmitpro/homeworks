import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import {Provider} from './context/Provider';
import {Navbar} from './client/components/Navbar';
import {ProductsList} from './client/components/ProductsList/components/ProductsList';
import {Modal} from './shared/components/Modal';
import {ShoppingCart} from './client/components/ShoppingCart/components/ShoppingCart';
import {EmptyCart} from './client/components/ShoppingCart/components/EmptyCard';

function App() {
  return (
      <Provider>
        <div className="App">
          <Navbar/>
          <ProductsList/>
          <Modal title="Cart">
            <EmptyCart/>
            <ShoppingCart/>
          </Modal>
        </div>
      </Provider>
  )
}

export default App;
