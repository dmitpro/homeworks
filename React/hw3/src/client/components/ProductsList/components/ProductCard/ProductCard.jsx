import React, {useContext} from 'react';
import {context} from '../../../../../context/context';
import './ProductCard.css'

export const ProductCard = (product) => {
  const {addToCart} = useContext(context);
  const {id, name, image, price} = product;
  return (
      <div key={id} className="col">
        <div className="card">
          <img className="card-img-top" src={image} alt="Card image_cap" />
          <div className="card-block">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">Price: ${price}</p>
            <button className="btn btn-primary" onClick={() => addToCart(id)}>
              Add to cart
            </button>
          </div>
        </div>
      </div>
  )
}