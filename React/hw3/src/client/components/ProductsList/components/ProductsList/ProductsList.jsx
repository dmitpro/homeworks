import React, {useContext} from 'react';
import {context} from '../../../../../context/context';
import {ProductCard} from '../ProductCard';

export const ProductsList = () => {
  const {products} = useContext(context).state;
  const productsElements = products.map(elem => <ProductCard key={elem.id} {...elem}/>)

  return (
      <div className="container">
        <h1>Products:</h1>
        <div className="row">{productsElements}</div>
      </div>
  )
}