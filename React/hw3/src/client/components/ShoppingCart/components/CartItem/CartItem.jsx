import React, {useContext} from 'react';
import {context} from '../../../../../context/context';
import './CartItem.css';

export const CartItem = (props) => {
  const {addToCart, removeFromCart, deleteItem, onChangeCount} = useContext(context);
  const {id, name, price, image, count, total} = props;
  return (
      <tr>
        <th scope="row">
          <div>
            <img src={image} width="80px" height="80px" alt="Item"/>
          </div>
          {name}
        </th>
        <td>${price}</td>
        <td>
          <div className="input-group">
            <button onClick={() => removeFromCart(id)} className="btn btn-primary"> - </button>
            <input type="number" onChange={e => onChangeCount(e.target.value, id)}
                   className="form-control" name={name} value={count} style={{width: "30px"}}
            />
            <button onClick={() => addToCart(id)} className="btn btn-primary"> + </button>
          </div>
        </td>
        <td>${total}</td>
        <td>
          <button className="btn btn-danger" onClick={() => deleteItem(id)}> х </button>
        </td>
      </tr>
  )
}
