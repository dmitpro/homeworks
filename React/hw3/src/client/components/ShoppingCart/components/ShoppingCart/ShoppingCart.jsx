import React, {useContext} from 'react';
import {context} from '../../../../../context/context';
import {CartItem} from '../CartItem';
import './ShoppingCart.css'

export const ShoppingCart = () => {
  const {shoppingCart} = useContext(context).state;
  const className = (shoppingCart.length > 0) ? 'shopping-cart show' : 'shopping-cart'
  const totalPrice = Number(shoppingCart.reduce((sum, item) => sum + item.total, 0).toFixed(2));
  const shoppingCartElements = shoppingCart.map(elem => <CartItem key={elem.id} {...elem} />)

  return (
      <div className={className}>
        <table className="show-cart table">
          <thead>
          <tr>
            <th>Item</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {shoppingCartElements}
          </tbody>
        </table>
        <hr/>
        <div>Total price: $<span className="total-cart">{totalPrice}</span></div>
      </div>
  )
}