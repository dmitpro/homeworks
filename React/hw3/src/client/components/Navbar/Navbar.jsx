import React, {useContext} from 'react';
import {context} from '../../../context/context';

export const Navbar = () => {
    const {state: {shoppingCart}, openModal, clearCart} = useContext(context);
    const totalCount = shoppingCart.reduce((sum, item) => sum + item.count, 0);

    return (
        <nav className="navbar fixed-top bg-faded">
            <div className="row">
                <div className="col">
                    <button type="button" onClick={openModal} className="btn btn-primary">
                        Cart (<span className="total-count">{totalCount}</span>)
                    </button>
                    <button onClick={clearCart} className="btn btn-danger">Clear Cart</button>
                </div>
            </div>
        </nav>
    )
}