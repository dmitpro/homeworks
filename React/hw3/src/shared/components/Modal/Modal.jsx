import React, {useContext} from 'react';
import {context} from '../../../context/context';
import './Modal.css';

export const Modal = ({title, children}) => {
  const {state: {isOpenModal}, closeModal} = useContext(context);
  const className = isOpenModal ? 'modal show' : 'modal';
  return (
      <div className={className}>
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{title}</h5>
              <button onClick={closeModal} type="button" className="close">
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {children}
            </div>
            <div className="modal-footer">
              <button onClick={closeModal} type="button" className="btn btn-secondary">Close</button>
            </div>
          </div>
        </div>
      </div>
  )
}