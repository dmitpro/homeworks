import {
  ADD_TO_CART,
  CLEAR_CART,
  CLOSE_MODAL,
  DELETE_ITEM,
  OPEN_MODAL,
  REMOVE_FROM_CART,
  ON_CHANGE_COUNT
} from './constants';

const updateCart = (shoppingCart, item, idx) => {
  console.log(item.count)
  if (item.count === 0) {
    return [...shoppingCart.slice(0, idx), ...shoppingCart.slice(idx + 1)];
  }

  if (idx === -1) {
    return [...shoppingCart, item];
  }

  return [...shoppingCart.slice(0, idx), item, ...shoppingCart.slice(idx + 1)];
};

const updateCartItem = (products, item = {}, quantity) => {
  const {
    id = products.id,
    name = products.name,
    price = products.price,
    image = products.image,
    count = 0,
    total = 0,
  } = item;
  return {
    id,
    name,
    price,
    image,
    count: count + quantity,
    total: Number((total + quantity * products.price).toFixed(2)),
  };
};

const updateOrder = (state, productId, quantity) => {
  const {products, shoppingCart} = state;
  const merch = products.find(({id}) => id === productId);
  const itemIndex = shoppingCart.findIndex(({id}) => id === productId);
  const item = shoppingCart[itemIndex];
  const newCartItem = updateCartItem(merch, item, quantity);
  return updateCart(shoppingCart, newCartItem, itemIndex);
}

export const reducer = (state, {type, payload}) => {
  switch (type) {
    case ADD_TO_CART: {
      const newShoppingCart = updateOrder(state, payload, 1);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case REMOVE_FROM_CART: {
      const newShoppingCart = updateOrder(state, payload, -1);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case DELETE_ITEM: {
      const item = state.shoppingCart.find(({id}) => id === payload);
      const newShoppingCart = updateOrder(state, payload, -item.count);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case ON_CHANGE_COUNT: {
      const item = state.shoppingCart.find(({id}) => id === payload.idx);
      const newShoppingCart = updateOrder(state, payload.idx, payload.value - item.count);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case CLEAR_CART:
      localStorage.setItem('shoppingCart', '');
      return {...state, shoppingCart: []};
    case OPEN_MODAL:
      return {...state, isOpenModal: true};
    case CLOSE_MODAL:
      return {...state, isOpenModal: false};
    default:
      return state;
  }
}