import {products} from './products';

const shoppingCart = localStorage.getItem('shoppingCart') || '[]';

export const initialState = {
  products: products || [],
  shoppingCart: JSON.parse(shoppingCart),
  count: 0,
  total: 0,
  isOpenModal: false
}