import React, {useReducer} from 'react';
import {initialState} from './initialState';
import {reducer} from './reducer';
import {context} from './context';
import {
  ADD_TO_CART,
  CLEAR_CART,
  CLOSE_MODAL,
  DELETE_ITEM,
  ON_CHANGE_COUNT,
  OPEN_MODAL,
  REMOVE_FROM_CART
} from './constants';

export const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = {
    state,
    addToCart: (idx) => dispatch({type: ADD_TO_CART, payload: idx}),
    removeFromCart: (idx) => dispatch({type: REMOVE_FROM_CART, payload: idx}),
    deleteItem: (idx) => dispatch({type: DELETE_ITEM, payload: idx}),
    onChangeCount: (value, idx) => dispatch({type: ON_CHANGE_COUNT, payload: {value, idx}}),
    clearCart: () => dispatch({type: CLEAR_CART}),
    openModal: () => dispatch({type: OPEN_MODAL}),
    closeModal: () => dispatch({type: CLOSE_MODAL}),
  }

  return (
      <context.Provider value={value}>
        {children}
      </context.Provider>
  )
}