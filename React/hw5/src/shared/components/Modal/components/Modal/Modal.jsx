import React from 'react';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';

import {closeModal} from '../../../../../app/store/actionCreators/modalActionCreators';

import './Modal.css';

export const Modal = ({title, children}) => {
  const {isOpenModal} = useSelector(state => state.modal, shallowEqual())
  const className = isOpenModal ? 'modal-center show' : 'modal-center';

  const dispatch = useDispatch();
  return (
      <div className={className}>
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{title}</h5>
              <button onClick={() => dispatch(closeModal())} type="button" className="close">
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {children}
            </div>
            <div className="modal-footer">
              <button onClick={() => dispatch(closeModal())} type="button" className="btn btn-secondary">Close</button>
            </div>
          </div>
        </div>
      </div>
  )
}