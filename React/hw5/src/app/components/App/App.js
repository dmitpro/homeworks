import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react'

import {Navbar} from '../../../client/components/Navbar';
import {ProductsList} from '../../../client/components/ProductsList/components/ProductsList';
import {Modal} from '../../../shared/components/Modal/components/Modal';
import {ShoppingCart} from '../../../client/components/ShoppingCart/components/ShoppingCart';
import {EmptyCart} from '../../../client/components/ShoppingCart/components/EmptyCard';
import {store, persistor} from '../../store/configureStore';

import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

function App() {
  return (
    <Provider store={store}><Modal title="Cart">
      <EmptyCart/>
      <ShoppingCart/>
    </Modal>
      <PersistGate loading={null} persistor={persistor}>
        <Navbar/>
        <ProductsList/>

      </PersistGate>
    </Provider>
  )
}

export default App;
