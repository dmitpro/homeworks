export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const DELETE_ITEM = 'DELETE_ITEM';
export const ON_CHANGE_COUNT = 'ON_CHANGE_COUNT';
export const CLEAR_CART = 'CLEAR_CART';

