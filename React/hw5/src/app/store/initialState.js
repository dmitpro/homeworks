import {products} from './products';

export const initialState = {
  products: products || [],
  shoppingCart: [],
  count: 0,
  total: 0,
}