import {combineReducers} from 'redux';

import {modalReducer} from './modalReducer';
import {cartReducer} from './cartReducer';

export const rootReducer = combineReducers({modal: modalReducer, cart: cartReducer});

