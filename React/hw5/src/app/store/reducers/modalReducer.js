import {CLOSE_MODAL, OPEN_MODAL} from '../modalActionTypes';

const initialState = {
    isOpenModal: false
};

export const modalReducer = (state = initialState, {type}) => {
    switch (type) {
        case OPEN_MODAL:
            return {...state, isOpenModal: true};
        case CLOSE_MODAL:
            return {...state, isOpenModal: false};
        default:
            return state;
    }
}