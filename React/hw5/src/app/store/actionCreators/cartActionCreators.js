import {
  ADD_TO_CART,
  CLEAR_CART,
  DELETE_ITEM,
  ON_CHANGE_COUNT,
  REMOVE_FROM_CART
} from '../cartActionsTypes';

export const addToCard = (payload) => {
  return {type: ADD_TO_CART, payload}
};

export const clearCard = (payload) => {
  return {type: CLEAR_CART, payload}
};

export const deleteItem = (payload) => {
  return {type: DELETE_ITEM, payload}
};

export const changeCount = (value, idx) => {
  return {type: ON_CHANGE_COUNT, payload: {value, idx}}
};

export const removeFromCard = (payload) => {
  return {type: REMOVE_FROM_CART, payload}
};
