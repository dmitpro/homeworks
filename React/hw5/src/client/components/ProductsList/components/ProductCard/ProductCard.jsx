import React from 'react';
import {useDispatch} from 'react-redux';

import {addToCard} from '../../../../../app/store/actionCreators/cartActionCreators';

import './ProductCard.css'

export const ProductCard = (product) => {
  const {id, name, image, price} = product;
  const dispatch = useDispatch();

  return (
    <div key={id} className="col">
      <div className="card">
        <img className="card-img-top" src={image} alt="Card image_cap"/>
        <div className="card-block">
          <h4 className="card-title">{name}</h4>
          <p className="card-text">Price: ${price}</p>
          <button className="btn btn-primary" onClick={() => dispatch(addToCard(id))}>
            Add to cart
          </button>
        </div>
      </div>
    </div>
  )
}