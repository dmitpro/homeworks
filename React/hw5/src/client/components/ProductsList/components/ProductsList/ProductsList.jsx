import React from 'react';
import {shallowEqual, useSelector} from 'react-redux';

import {ProductCard} from '../ProductCard';

export const ProductsList = () => {
  const {products} = useSelector(state => state.cart, shallowEqual());
  const productsElements = products.map(elem => <ProductCard key={elem.id} {...elem}/>)

  return (
    <div className="container">
      <h1>Products:</h1>
      <div className="row">{productsElements}</div>
    </div>
  )
}