import React from 'react';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';

import {openModal} from '../../../app/store/actionCreators/modalActionCreators';
import {clearCard} from '../../../app/store/actionCreators/cartActionCreators';

export const Navbar = () => {
  const {shoppingCart} = useSelector(state => state.cart, shallowEqual());
  const totalCount = shoppingCart.reduce((sum, item) => sum + item.count, 0);
  const dispatch = useDispatch();

  return (
    <nav className="navbar fixed-top bg-faded">
      <div className="row">
        <div className="col">
          <button type="button" onClick={() => dispatch(openModal())} className="btn btn-primary">
            Cart (<span className="total-count">{totalCount}</span>)
          </button>
          <button onClick={() => dispatch(clearCard())} className="btn btn-danger">Clear Cart</button>
        </div>
      </div>
    </nav>
  )
}