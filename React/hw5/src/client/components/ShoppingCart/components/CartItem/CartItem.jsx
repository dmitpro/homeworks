import React from 'react';
import {useDispatch} from 'react-redux';

import './CartItem.css';

import {
  addToCard,
  changeCount,
  deleteItem,
  removeFromCard
} from '../../../../../app/store/actionCreators/cartActionCreators';

export const CartItem = (props) => {
  const {id, name, price, image, count, total} = props;
  const dispatch = useDispatch();

  return (
    <tr>
      <th scope="row">
        <div>
          <img src={image} width="80px" height="80px" alt="Item"/>
        </div>
        {name}
      </th>
      <td>${price}</td>
      <td>
        <div className="input-group">
          <button onClick={() => dispatch(removeFromCard(id))} className="btn btn-primary"> -</button>
          <input type="number" onChange={e => dispatch(changeCount(e.target.value, id))}
                 className="form-control" name={name} value={count} style={{width: "30px"}}
          />
          <button onClick={() => dispatch(addToCard(id))} className="btn btn-primary"> +</button>
        </div>
      </td>
      <td>${total}</td>
      <td>
        <button className="btn btn-danger" onClick={() => dispatch(deleteItem(id))}> х</button>
      </td>
    </tr>
  )
}
