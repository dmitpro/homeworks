import React from 'react';
import {shallowEqual, useSelector} from 'react-redux';

import './EmptyCart.css'

export const EmptyCart = () => {
  const {shoppingCart} = useSelector(state => state.cart, shallowEqual());
  const className = (shoppingCart.length > 0) ? 'empty-cart' : 'empty-cart show'

  return (
    <h6 className={className}>Your cart is empty</h6>
  )
}