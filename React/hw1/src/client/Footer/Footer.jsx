import React from 'react';
import { SocialIcons } from '../../shared/components/SocialIcons';
import './Footer.css'
import { MenuLink } from '../../shared/components/MenuLink';
import { Logo } from '../../shared/components/Logo';
import { Copyright } from '../../shared/components/Copyright';

export const Footer = (props) => {
  return (
    <div className='footer'>
      <div className="footer-container">
        <MenuLink {...props.menuLink} />
        <Logo {...props.logo} />
        <SocialIcons {...props.social} />
      </div>
      <Copyright {...props.logoSmall} />
    </div>
  )
}
