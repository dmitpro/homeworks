import React from 'react';
import './Navbar.css'
import { Logo } from '../../../../shared/components/Logo';
import { Auth } from '../../../../shared/components/Auth';

export const Navbar = (props) => {
  return (
    <div className="navbar">
      <Logo {...props}/>
      <Auth />
    </div>
  )
}
