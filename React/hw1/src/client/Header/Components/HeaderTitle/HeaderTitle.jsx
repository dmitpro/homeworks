import React from 'react';
import { Categories } from '../../../../shared/components/Categories';
import { Duration } from '../../../../shared/components/Duration';
import './HeaderTitle.css'

export const HeaderTitle = (props) => {
  const duration = props.duration.split(':');
  return (
    <div className="headerTitle">
      <h2 className="title-text">{props.title}</h2>
      <Categories {...props.categories}/>  | {'\u00A0'}
      <Duration {...duration} />
    </div>
  )
}
