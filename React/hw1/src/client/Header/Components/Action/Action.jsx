import React from 'react';
import './Action.css'
import { Button } from '../../../../shared/components/Button';

export const Action = () => {
  return (
    <div className="action">
      <Button className="btn" text="Watch Now" />
      <Button className="btn-black" text="View info"/>
      <Button className="btn-transp" text="+ Favorites"/>
      <img className="more-icon" src="/More@0.5X.png" alt="More" />
    </div>
  )
}
