import React from 'react';
import './Header.css'
import { Navbar } from './Components/Navbar';
import { Pagination } from '../../shared/components/Pagination';
import { HeaderTitle } from './Components/HeaderTitle';
import { Action } from './Components/Action';
import { StarRating } from '../../shared/components/StarRating';
import { RatingIndex } from '../../shared/components/RatingIndex';

export const Header = (props) => {
  const {logo, title, starRating, ratingIndex, categories, duration, paginator}= props;
  const headerTitle = {
      title:title,
      categories: categories,
      duration: duration
  }
  return (
    <div className='header'>
      <Navbar {...logo} />
      <Pagination pageIdx={paginator}/>
      <HeaderTitle {...headerTitle}  />
      <div className="header-bottom">
        <div className="rating">
          <StarRating rating={starRating} />
          <RatingIndex index={ratingIndex} />
        </div>
        <Action />
      </div>
    </div>
  )
}
