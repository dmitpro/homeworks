import React from 'react';
import './MainContainer.css'
import { FilterMenu } from './FilterMenu';
import { Gallery } from './Gallery';
import { Loader } from '../../shared/components/Loader';

export const MainContainer = (props) => {
  return (
    <div className="main-container">
      <FilterMenu {...props}/>
      <Gallery {...props} />
      <Loader />
    </div>
  )
}
