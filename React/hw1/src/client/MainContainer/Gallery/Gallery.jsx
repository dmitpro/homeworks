import React from 'react';
import './Gallery.css'
import { Card } from './Card';
import { Banner } from '../../../shared/components/Banner';

export const Gallery = (props) => {
  const galleryContent = props.gallery.map((elem, idx) => <Card {...elem} key={idx} />);
  const galleryBeforeBanner = galleryContent.slice(0, 12);
  const galleryAfterBanner = galleryContent.slice(12);

  return (
    <div>
      <div className="gallery">
        {galleryBeforeBanner}
      </div>
      <Banner {...props.banner} />
      <div className="gallery">
        {galleryAfterBanner}
      </div>
    </div>
  )
};
