import React from 'react';
import './Card.css'
import { RatingIndex } from '../../../../shared/components/RatingIndex';
import { Categories } from '../../../../shared/components/Categories';

export const Card = (props) => {
  const {poster, title, ratingIndex, categories} = props;
  const bgStyle = {
    backgroundImage: "url(" + poster + ")"
  };
  return (
    <div className="card">
      <div className="card-img-top" style={bgStyle}></div>
      <div className="card-description">
        <h4 className="title">{title}</h4>
        <RatingIndex index={ratingIndex} />
      </div>
      <div className="card-categories">
        <Categories {...categories} />
      </div>
    </div>
  )
};
