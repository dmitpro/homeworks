import React from 'react';
import './FilterMenu.css';

export const FilterMenu = (props) => {
  const menuList = props.menuList.map((elem, i) => <li className="menu-item" key={i}>
    <a className="filter-menu-link" href="#">{elem}</a></li>);
  return (
    <div className="filter-menu">
      <ul className="menu">
        {menuList}
      </ul>
      <div className="view-mode">
        <img src="/Preview-mode@0.5X.png" alt="Preview mode" />
        <img src="/Table-mode@0.5X-2.png" alt="Table mode" />
      </div>
    </div>
  )
}
