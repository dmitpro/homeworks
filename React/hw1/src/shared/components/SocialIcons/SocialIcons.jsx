import React from 'react';
import './SocialIcons.css'

export const SocialIcons = (props) => {
  const icons = Object.values(props).map((elem, idx) => <a href={elem.link} key={idx} target="_blank">
    <i className={`${elem.name} icon`}></i></a>);
  return (
    <div className="social-icons">
      {icons}
    </div>
  )
};
