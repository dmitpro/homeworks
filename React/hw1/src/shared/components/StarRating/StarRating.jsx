import React from 'react';
import './StarRating.css'

export const StarRating = (props) => {
  const ratingArr = new Array (props.rating).fill(null);
  const stars = ratingArr.map((elem, i) => <img src="/star-fill.svg" key={i} className="star"/>)
  return (
    <div className="star-rating">
      {stars}
    </div>
  )
}
