import React from 'react';
import { Button } from '../Button';
import './Auth.css';
import { Search } from '../Search';

export const Auth = () => {
  return (
    <div>
      <Search />
      <Button className="btn-transp" text="Sign in" />
      <Button className="btn" text="Sign Up" />
    </div>
  )
}
