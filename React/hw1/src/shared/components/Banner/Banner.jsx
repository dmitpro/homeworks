import React from 'react';
import './Banner.css'
import { Button } from '../Button';

export const Banner = (props) => {
  return (
    <div className="banner">
      <h2 className="banner-header">{props.header}</h2>
      <Button className="btn" text={props.btnText} />
    </div>
  )
}
