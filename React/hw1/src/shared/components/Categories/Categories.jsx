import React from 'react';
import './Categories.css'

export const Categories = (props) => {
  console.log(props)
  const categorieslist = Object.values(props).map((elem, i) => <li className="categories-item" key={i}>{elem}{props.separ}</li>);
  return (
    <ul className='categories'>
      {categorieslist}
    </ul>
  )
};
