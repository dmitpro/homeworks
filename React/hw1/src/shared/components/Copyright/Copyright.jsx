import React from 'react';
import './Copyright.css'
import { Logo } from '../Logo';

export const Copyright = (props) => {
  return (
    <div className='copyright'>
      <span>Copyright © 2017 </span><Logo {...props} /><span>. All Rights Reserved.</span>
    </div>
  )
}
