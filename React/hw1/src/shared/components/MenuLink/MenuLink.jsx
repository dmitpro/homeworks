import React from 'react';
import './MenuLink.css'

export const MenuLink = (props) => {
  const menuLinkContent = Object.values(props).map((elem, i) => <li key={i}><a href="#" >{elem}</a></li>);
  return (
    <ul className='menu-link'>
      {menuLinkContent}
    </ul>
  )
}
