import React from 'react';
import './Search.css'

export const Search = () => {
  return (
    <a className="search-link" href="#">
      <i className="fas fa-search" ></i>
    </a>
  );
};
