import React from 'react';
import './Duration.css'

export const Duration = (props) => {
  return (
    <span className="duration">
      {` ${props[0]}h ${props[1]}m`}
    </span>
  )
}
