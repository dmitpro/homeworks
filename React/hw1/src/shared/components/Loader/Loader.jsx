import React from 'react';
import './Loader.css'

export const Loader = () => {
  return (
    <div className="loader">
      <img src="/Loading Icon@0.5X.png" alt="Loading" />
      <p className="loader-text">LOADING</p>
    </div>
  )
}
