import React from 'react';
import './RatingIndex.css'

export const RatingIndex = (props) => {
  return (
    <div className="rating-index">
      {props.index}
    </div>
  )
}
