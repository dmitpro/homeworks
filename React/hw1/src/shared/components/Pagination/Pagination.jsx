import React from 'react';
import './Pagination.css'

export const Pagination = (props) => {
  const idxArr = new Array (props.pageIdx).fill(null);
  const pageElem = idxArr.map((elem, i) => <div className="paginator-elem" key={i}><a href="#"></a></div>)
  return (
    <div className="paginator">
      {pageElem}
    </div>
  )
}
