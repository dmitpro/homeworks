import React from 'react';
import './Logo.css'

export const Logo = (props) => {
  const logoStyle = {
    color: props.color,
    fontSize: props.size,
  }
  return (
    <div className="logo">
      <a style={logoStyle} className="logo-link" href={props.logoUrl}>
        <span className="logo-left">{props.logo1}</span>
        <span className="logo-right">{props.logo2}</span>
      </a>
    </div>
  )
}
