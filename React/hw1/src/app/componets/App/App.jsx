import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/css/style.css';
import { Header } from '../../../client/Header';
import { Footer } from '../../../client/Footer';
import { MainContainer } from '../../../client/MainContainer';

const pageProps = {
  header: {
    logo: {
      logo1: 'MOVIE',
      logo2: 'rise',
      logoUrl: '#',
      color: 'white',
      size: '17px',
    },
    title: 'The Jungle Book',
    starRating: 5,
    ratingIndex: 4.8,
    categories: [' Adventure ', ' Drama ', ' Family ', ' Fantasy '],
    duration: '1:46',
    paginator: 5,
  },

  mainContainer: {
    menuList: ['Trending', 'Top Rated', 'New Arrivals', 'Trailers', 'Coming Soon', 'Genre \u2304'],

    gallery: [
      {
        title: 'Fantastic Beasts...',
        poster: '/fantastic_beasts_and_where_to_find_them_teaser_by_umbridge1986-d7jek45_large@0.5X.png',
        ratingIndex: 4.7,
        categories: [' Adventure', ' Family ', ' Fantasy ']
      },
      {
        title: 'AssAssin’s Creed',
        poster: '/4857-Edward-Kenway-Assassins-Creed-www.WallpaperMotion.com_@0.5X.png',
        ratingIndex: 4.2,
        categories: [' Action', ' Adventure', ' Fantasy']
      },
      {
        title: 'Now you see me 2',
        poster: '/nowyouseeme@0.5X.png',
        ratingIndex: 4.4,
        categories: [' Action', ' Adventure', ' Comedy']
      },
      {
        title: 'The Legend of Ta...',
        poster: '/tarzan@0.5X.png',
        ratingIndex: 4.3,
        categories: [' Action', ' Adventure', ' Drama']
      },
      {
        title: 'Doctor Strange',
        poster: '/doctorstrange1@0.5X.png',
        ratingIndex: 4.8,
        categories: [' Action', ' Adventure', ' Fantasy']
      },
      {
        title: 'Captain America...',
        poster: '/d0b27807-14bd-434a-9e8b-9eeb4550705b@0.5X.png',
        ratingIndex: 4.9,
        categories: [' Adventure', ' Adventure', ' Sci-Fi']
      },
      {
        title: 'Alice Through th...',
        poster: '/alice_through_the_looking_glass@0.5X.png',
        ratingIndex: 4.1,
        categories: [' Adventure', ' Family', ' Fantasy']
      },
      {
        title: 'Finding Dory',
        poster: '/finding-dory-teaser@0.5X.png',
        ratingIndex: 4.7,
        categories: [' Animation', ' Adventure', ' Comedy']
      },
      {
        title: 'The BFG',
        poster: '/the-bfg-new-poster-social@0.5X.png',
        ratingIndex: 3.2,
        categories: [' Adventure', ' Family', ' Fantasy']
      },
      {
        title: 'Independence Day',
        poster: '/independence-day-resurgence@0.5X.png',
        ratingIndex: 3.9,
        categories: [' Action', ' Sci-Fi']
      },
      {
        title: 'Ice Age: Collisio...',
        poster: '/ice-age-collision-course@0.5X.png',
        ratingIndex: 4.5,
        categories: [' Adventure', ' Comedy']
      },
      {
        title: 'Moana',
        poster: '/flex_moana_header_ddaba7de@0.5X.png',
        ratingIndex: 4.9,
        categories: [' Action', ' Fantasy']
      },
      {
        title: 'Captain America...',
        poster: '/d0b27807-14bd-434a-9e8b-9eeb4550705b@0.5X.png',
        ratingIndex: 4.9,
        categories: [' Adventure', ' Adventure', ' Sci-Fi']
      },
      {
        title: 'Doctor Strange',
        poster: '/doctorstrange1@0.5X.png',
        ratingIndex: 4.8,
        categories: [' Action', ' Adventure', ' Fantasy']
      },
      {
        title: 'AssAssin’s Creed',
        poster: '/4857-Edward-Kenway-Assassins-Creed-www.WallpaperMotion.com_@0.5X.png',
        ratingIndex: 4.2,
        categories: [' Action', ' Adventure', ' Fantasy']
      },
      {
        title: 'Fantastic Beasts...',
        poster: '/fantastic_beasts_and_where_to_find_them_teaser_by_umbridge1986-d7jek45_large@0.5X.png',
        ratingIndex: 4.7,
        categories: [' Adventure', ' Family ', ' Fantasy ']
      },
    ],

    banner: {
      header: 'Receive information on the latest hit movies straight to your inbox',
      btnColor: '#3BB4E0',
      btnText: 'Subscribe!'
    }
  },

  footer: {
    menuLink: ['About', 'Terms of Service', 'Contact'],
    logo: {
      logo1: 'MOVIE',
      logo2: 'rise',
      logoUrl: '#',
      color: '#0AAEE4',
      size: '17px',
    },
    logoSmall: {
      logo1: 'MOVIE',
      logo2: 'rise',
      logoUrl: '#',
      color: '#A7AAAB',
      size: '12px',
    },
    social: [
      {
        name: "fab fa-facebook-f",
        link: "https://facebook.com"
      },
      {
        name: "fab fa-twitter",
        link: "https://twitter.com",
      },
      {
        name: "fab fa-pinterest",
        link: "https://pinterest.com"
      },
      {
        name: "fab fa-instagram",
        link: "https://instagram.com"
      },
      {
        name: "fab fa-youtube",
        link: "https://youtube.com"
      },
    ]
  }
};

export const App = () => {
    return (
        <div className="container">
            <Header {...pageProps.header} />
            <MainContainer {...pageProps.mainContainer} />
            <Footer {...pageProps.footer} />
        </div>
    );
};
