import React from 'react';
import {ErrorMessage, Field} from 'formik';

import {fields} from '../../../client/OrderForm/settings/fields';

import './fieldWithError.css'

const FieldWithError = ({
                          name,
                          className = null,
                          as = null,
                          children = null
                        }) => {

  return (
    <>
      <Field {...fields[name]} className={className} as={as}>
        {children}
      </Field>
      <ErrorMessage name={name}>
        {(errorMessage) => {
          return <p className="error">{errorMessage}</p>
        }}
      </ErrorMessage>
    </>
  )
}

export default FieldWithError;
