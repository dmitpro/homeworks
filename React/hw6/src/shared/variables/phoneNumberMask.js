const phoneNumberMask = [
  '+',
  '3',
  '8',
  '(',
  '0',
  /[0-9]/,
  /\d/,
  ')',
  ' ',
  /\d/,
  /\d/,
  /\d/,
  ' ',
  /\d/,
  /\d/,
  ' ',
  /\d/,
  /\d/
];

export default phoneNumberMask;