const validationsMessage = {
  required: 'Поле обязательно к заполнению',
  email: 'В email обязательно наличие @ и отсутствие пробелов',
  phone: 'Номер телефона должен быть введен полностью',
  warehouses: 'Выберите номер отделения',
  city: 'Выберите город',
  delivery: 'Выберите способ доставки'
}

export default validationsMessage;