import React from 'react';

import OrderForm from './client/OrderForm/components/OrderForm';

function App() {

  return (
    <div className="App">
      <OrderForm/>
    </div>
  );
}

export default App;
