const initialValues = {
  name: '',
  lastName: '',
  surname: '',
  phone: '',
  email: '',
  city: '',
  delivery: '',
  warehouses: '',
  street: '',
  house: '',
  apartment: '',
  deliveryTime: '',
  mailingActions: '',
  comments: '',
  submit: 'Оплатить'
};

export default initialValues;