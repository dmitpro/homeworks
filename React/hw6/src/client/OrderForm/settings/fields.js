export const fields = {
  name: {
    type: 'text',
    name: 'name',
    placeholder: 'Имя'
  },
  surname: {
    type: 'text',
    name: 'surname',
    placeholder: 'Отчество'
  },
  lastName: {
    type: 'text',
    name: 'lastName',
    placeholder: 'Фамилия'
  },
  phone: {
    type: 'tel',
    name: 'phone',
    placeholder: '+38 (0__) ___ __ __)'
  },
  email: {
    type: "email",
    name: "email",
    placeholder: "email"
  },
  city: {
    name: 'city',
    options: [{
      value: '',
      text: 'Город',
      disabled: true
    },{
      value: 'Киев',
      text: 'Киев'
    }, {
      value: 'Харьков',
      text: 'Харьков'
    }, {
      value: 'Днепр',
      text: 'Днепр'
    }, {
      value: 'Одесса',
      text: 'Одесса'
    }, {
      value: 'Львов',
      text: 'Львов'
    }]
  },
  delivery: {
    name: 'delivery',
    options: [{
      value: '',
      text: 'Способ доставки',
      disabled: true
    },{
      value: 'Новая почта',
      text: 'Новая почта'
    }, {
      value: 'Курьерская доставка',
      text: 'Курьерская доставка'
    }]
  },
  warehouses: {
    name: 'warehouses',
    options: [{
      value: '',
      text: 'Отделение',
      disabled: true,
    }, {
      value: '1',
      text: 'Отделение №1'
    }, {
      value: '2',
      text: 'Отделение №2'
    }, {
      value: '3',
      text: 'Отделение №3'
    }, {
      value: '4',
      text: 'Отделение №4'
    }]
  },
  street: {
    type: 'text',
    name: 'street',
    placeholder: 'Улица'
  },
  house: {
    type: 'text',
    name: 'house',
    placeholder: 'Дом'
  },
  apartment: {
    type: 'text',
    name: 'apartment',
    placeholder: 'Квартира'
  },
  deliveryTime: {
    type: 'text',
    name: 'deliveryTime',
    placeholder: 'Время доставки'
  },
  mailingActions: {
    type: 'checkbox',
    name: 'mailingActions'
  },
  comments: {
    type: 'textAria',
    name: 'comments'
  },
  submit: {
    type: 'submit',
    value: 'Оплатить',
  }
};