import initialValues from './initialValues';
import validationSchema from './validationSchema';

const onSubmit = (values, actions) => {
  console.log(values);
  actions.resetForm({initialValues})
};

const formikProps = {
  initialValues,
  validationSchema,
  onSubmit
}

export default formikProps;