import * as Yup from 'yup';
import validationsMessage from '../../../shared/variables/validationMessage';

const validationSchema = Yup.object().shape({
  name: Yup.string().required(validationsMessage.required),
  surname: Yup.string().required(validationsMessage.required),
  lastName: Yup.string().required(validationsMessage.required),
  phone: Yup.string()
    .matches('^[+38]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$', validationsMessage.phone)
    .required(validationsMessage.required),
  email: Yup.string()
    .email(validationsMessage.email)
    .required(validationsMessage.required),
  city: Yup.string().required(validationsMessage.city),
  delivery: Yup.string().required(validationsMessage.delivery),
  warehouses: Yup.string()
    .when('delivery', {
      is: 'Новая почта',
      then: Yup.string().required(validationsMessage.warehouses)
    }),
  street: Yup.string()
    .when('delivery', {
      is: 'Курьерская доставка',
      then: Yup.string().required(validationsMessage.required)
    }),
  house: Yup.string()
    .when('delivery', {
      is: 'Курьерская доставка',
      then: Yup.string().required(validationsMessage.required)
    }),
  apartment: Yup.string()
    .when('delivery', {
      is: 'Курьерская доставка',
      then: Yup.string().required(validationsMessage.required)
    }),
  deliveryTime: Yup.string()
    .when('delivery', {
      is: 'Курьерская доставка',
      then: Yup.string().required(validationsMessage.required)
    })
})

export default validationSchema;
