import React from 'react';
import {Formik, Field, Form} from 'formik';
import MaskedInput from 'react-text-mask';

import {fields} from '../../settings/fields';
import phoneNumberMask from '../../../../shared/variables/phoneNumberMask';
import formikProps from '../../settings/formikProps';
import FieldWithError from '../../../../shared/components/FieldWithError';

import './orderForm.css'

const OrderForm = () => {

  return (
    <div className="card white center-align">
      <span className="card-title green-text darken-2">Оформление заказа</span>
      <div className="card-content">
        <Formik  {...formikProps}>
          {formik => {
            return (
              <Form id="order-form">
                <FieldWithError name="name"/>
                <FieldWithError name="surname"/>
                <FieldWithError name="lastName"/>
                <FieldWithError name="phone" children={
                  ({field}) => (
                    <MaskedInput
                      {...field}
                      mask={phoneNumberMask}
                      placeholder={fields.phone.placeholder}
                    />
                  )}
                />
                <FieldWithError name={'email'}/>
                <FieldWithError className="input-field" as="select" name="city" children={
                  fields.city.options.map(item =>
                    <option key={item.value} {...item}>{item.text}</option>)}
                />
                <FieldWithError className="input-field" as="select" name="delivery" children={
                  fields.delivery.options.map(item =>
                    <option key={item.value} {...item}>{item.text}</option>)}
                />
                {formik.values.delivery === 'Новая почта' &&
                <FieldWithError className="input-field" name="warehouses" as="select" children={
                  fields.warehouses.options.map(item =>
                    <option key={item.value} {...item}>{item.text}</option>)}
                />}
                {formik.values.delivery === 'Курьерская доставка' &&
                <>
                  <FieldWithError name='street'/>
                  <FieldWithError name='house'/>
                  <FieldWithError name='apartment'/>
                  <FieldWithError name='deliveryTime'/>
                </>}
                <label>
                  <Field className="filled-in" {...fields.mailingActions}/>
                  <span className="action-text"> подписаться на рассылку акций</span>
                </label>
                <p className="left-align">Ваш комментарий:</p>
                <Field className="comments" as="textarea" name="comments"/>
                <Field
                  className='btn waves-effect waves-light'
                  disabled={!(formik.isValid && formik.dirty)}
                  {...fields.submit}
                />
              </Form>
            )
          }}
        </Formik>
      </div>
    </div>
  );
};

export default OrderForm;