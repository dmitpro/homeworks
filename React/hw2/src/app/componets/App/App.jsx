import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Header } from '../../../client/Header';
import { AddBookForm } from '../../../client/AddBookForm';
import { BookList } from '../../../client/BookList';

const pageProps = {
  header: {
    logo: {
      logoLeft: ' Book',
      logoRight: ' List'
    },
    description: 'Add your book information to store it in database'
  },

  inputBook : {
    title: {
      htmlFor: 'title',
      label: 'Title',
      type: 'text',
      name: 'title',
      className: 'form-control'
    },
    author: {
      htmlFor: 'author',
      label: 'Author',
      type: 'text',
      name: 'author',
      className: 'form-control'
    },
    isbn: {
      htmlFor: 'isbn',
      label: 'ISBN#',
      type: 'text',
      name: 'isbn',
      className: 'form-control'
    },
    submit: {
      type: 'submit',
      value: 'Add Book',
      className: 'btn btn-primary'
    },
  },
};


export class App extends Component {
  state = {
    books: [
      {
        dataId: 12,
        title: 'Малазанская книга Падших',
        author: 'Стивен Эриксон',
        isbn: '978-3-16-148410-0',
      }
    ]
  };

  addBook = (title, author, isbn) => {
    const booksArr = this.state.books;
    const dataId = booksArr.length > 0 ? booksArr[booksArr.length - 1].dataId + 1 : 1;
    const newBook = {dataId, title, author, isbn};
    this.setState(({books}) => {
      return {
        books: [...books, newBook]
      }
    })
  };

  updateBook = (dataId, title, author, isbn) =>{
    this.setState(({books})=>{
      const newBooks = [...books];
      const idx = newBooks.findIndex(item => item.dataId === dataId);
      newBooks[idx] = {dataId, title, author, isbn};
      return{
        books: newBooks
      }
    })
  };

  deleteBook = (index) => {
    this.setState(({books})=>{
      const newBooks = [...books];
      const idx = newBooks.findIndex(item => item.dataId === index);
      newBooks.splice(idx, 1);
      return{
        books: newBooks
      }
    })
  };

  render() {
    const {header, inputBook} = pageProps;
    return (
      <div className="container">
        <Header {...header} />
        <AddBookForm {...inputBook} addBook={this.addBook} />
        <BookList books={this.state.books} updateBook={this.updateBook} deleteBook={this.deleteBook} />
      </div>
    );
  };
}
