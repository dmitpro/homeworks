import React from 'react';
import { BookItem } from './BookItem';

export const BookList = (props) => {
  const booksArr = props.books;
  const booksList = booksArr.map(elem => <BookItem key={elem.dataId} {...elem} deleteBook={props.deleteBook} updateBook={props.updateBook} />);
  return (
    <div>
      <h3 id="book-count" className="book-count mt-5">Books total: {booksArr.length}</h3>
      <table className="table table-striped mt-2">
        <thead>
        <tr>
          <th>Title</th>
          <th>Author</th>
          <th>ISBN#</th>
          <th colSpan="2" style={{width: "10%"}}>Action</th>
        </tr>
        </thead>
        <tbody>
        {booksList}
        </tbody>
      </table>
    </div>
  )
};

