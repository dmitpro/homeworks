import React, { Component } from 'react';

export class BookItem extends Component {

  state = {
    edit: false,
    dataId: this.props.dataId,
    title: this.props.title,
    author: this.props.author,
    isbn: this.props.isbn
  };

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  handleEdit = (e) => {
    e.preventDefault();
    this.setState({edit: true});
  };

  handleUpdate = (e) => {
    e.preventDefault();
    const {dataId, title, author, isbn} = this.state;
    this.props.updateBook(dataId, title, author, isbn);
    this.setState({edit: false});
  };

  handleDelete = (e) => {
    e.preventDefault();
    const {dataId, deleteBook} = this.props;
    deleteBook(dataId);
  };

  render() {
    if (!this.state.edit) {
      const {title, author, isbn, dataId} = this.props;
      return (
        <tr data-id={dataId}>
          <td>{title}</td>
          <td>{author}</td>
          <td>{isbn}</td>
          <td><a onClick={this.handleEdit} href="#" className="btn btn-info btn-sm"><i className="fas fa-edit"></i></a></td>
          <td><a onClick={this.handleDelete} href="#" className="btn btn-danger btn-sm btn-delete">X</a></td>
        </tr>
      )
    } else {
      const {title, author, isbn, dataId} = this.state;
      return (
        <tr data-id={dataId}>
          <td><input onChange={this.handleChange} type="text" value={title} name="title" required /></td>
          <td><input onChange={this.handleChange} type="text" value={author} name="author" required /></td>
          <td><input onChange={this.handleChange} type="text" value={isbn} name="isbn" required /></td>
          <td><input onClick={this.handleUpdate} type="submit" value="Update" className="btn btn-primary" /></td>
        </tr>
      )
    }
  }
}

