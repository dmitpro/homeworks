import React, { Component } from 'react';
import { InputBook } from './InputBook/InputBook';

export class AddBookForm extends Component {

  state = {
    title: '',
    author: '',
    isbn: '',
  };

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const {title, author, isbn} = this.state;
    this.props.addBook(title, author, isbn);
    this.setState({title: '', author: '', isbn: ''})
  };

  render() {
    const {title, author, isbn, submit} = this.props;
    return (
      <div className="row">
        <div className="col-lg-4">
          <form onSubmit={this.handleSubmit} id="add-book-form">
            <InputBook {...title} value={this.state.title} handleChange={this.handleChange} />
            <InputBook {...author} value={this.state.author} handleChange={this.handleChange} />
            <InputBook {...isbn} value={this.state.isbn} handleChange={this.handleChange} />
            <InputBook {...submit} />
          </form>
        </div>
      </div>
    )
  }
}
