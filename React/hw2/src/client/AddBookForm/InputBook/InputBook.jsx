import React from 'react';
import './InputBook.css'

export const InputBook = (props) => {
  const {label, htmlFor, className, type, name, value, handleChange} = props;
  return (
    <div className="form-group">
      <label htmlFor={htmlFor}>{label}</label>
      <input className={className} type={type} name={name} value={value} required onChange={handleChange} />
    </div>
  )
};
