import React from 'react';

export const Header = (props) => {
  const {logo, description} = props;
  return (
    <div>
      <h1 className="display-4 text-center"><i className="fas fa-book-open text-primary"></i>
        <span className="text-secondary">{logo.logoLeft}</span>{logo.logoRight}</h1>
      <p className="text-center">{description}</p>
    </div>
  )
};

