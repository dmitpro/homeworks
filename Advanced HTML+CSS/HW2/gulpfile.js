const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify-es').default;
const imageMin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

const path = {
    css: './src/scss/**/*.scss',
    js: './src/js/**/*.js',
    img: './src/img/**/*.{png,jpg,jpeg,gif,svg}',
    html: './src/*.html',
};

const style = () =>
    gulp.src(path.css).
    pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)).
    pipe(autoprefixer(['last 4 versions'], {cascade: false})).
    pipe(concat('styles.min.css')).
    pipe(cleanCSS({
      level: 2
    })).
    pipe(gulp.dest('./dist/css/')).
    pipe(browserSync.stream());

const js = () =>
    gulp.src(path.js).
    pipe(concat('script.min.js')).
    pipe(uglify()).
    pipe(gulp.dest('./dist/js/')).
    pipe(browserSync.stream());

const img = () =>
    gulp.src(path.img).
    pipe(imageMin([
      imageMin.gifsicle({interlaced: true}),
      imageMin.mozjpeg({quality: 75, progressive: true}),
      imageMin.optipng({optimizationLevel: 5}),
      imageMin.svgo({
        plugins: [
          {removeViewBox: true},
          {cleanupIDs: false}
        ]
      })
    ])).
    pipe(gulp.dest('./dist/img/')).
    pipe(browserSync.stream());

const html = () =>
    gulp.src(path.html).
    pipe(gulp.dest('./dist/')).
    pipe(browserSync.stream());

const cleandev = () =>
    gulp.src('./dist/*', {read: false}).
    pipe(clean());

const watch = () => {
    browserSync.init({
      server: {
        baseDir: './dist/'
      },
    });

  gulp.watch(path.css, style);
  gulp.watch(path.js, js);
  gulp.watch(path.html, html)
};

gulp.task('cleandev', cleandev);
gulp.task('watch', watch);
gulp.task('build', gulp.series('cleandev', gulp.parallel(html, style, js, img)));
gulp.task('dev', gulp.series('build', watch));
