const createNewUser = function() {
  let firstName;
  let lastName;
  let birthday;
  let correct;

  while (!correct) {
    firstName = prompt('Enter first name:', firstName);
    lastName = prompt('Enter last name:', lastName);
    birthday = prompt('Enter day of birthday:', 'dd.mm.yyyy');
    if (   firstName
        && lastName
        && birthday
        && birthday.match(/^(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)[0-9]{2}$/)) {
      correct = true;
    } else {
     alert ('Enter correct information');
    }
  }

  return newUser = {
    firstName,
    lastName,
    birthday,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge() {
      let birthDateUS = this.birthday.slice(3,6) + this.birthday.slice(0,3) + this.birthday.slice(6,10);
      return Math.floor((new Date().getTime() - Date.parse(birthDateUS)) / (24 * 3600 * 365.25 * 1000));
    },
    getPassword() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6,10);
    },
  };
};

const user1 = createNewUser();
console.log(user1);
console.log(`${user1.firstName} ${user1.lastName} is ${user1.getAge()} years old`);
console.log(`Login: ${user1.getLogin()}`);
console.log(`Password : ${user1.getPassword()}`);