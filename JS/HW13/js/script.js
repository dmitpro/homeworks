const button = document.querySelector('.change-btn');
const links = document.getElementsByTagName('a');
const cardDetails = document.querySelectorAll('.card-details');
const cards = document.querySelectorAll('.card-img');

let orangeTheme = {
  background: 'url("img/background.jpg")',
  fontColor: 'rgb(255, 255, 255)',
  linkColor: 'rgb(255, 255, 255)',
  cardDetailsColor: 'rgb(242, 101, 34)',
  borderGradientColor: 'linear-gradient(to bottom, white, black)',
};

let blueTheme = {
  background: 'url("img/Milano-bg.jpg")',
  fontColor: 'rgb(250, 230, 110)',
  linkColor: 'rgb(250, 230, 110)',
  cardDetailsColor: 'rgb(80, 150, 255)',
  borderGradientColor: 'linear-gradient(to bottom, yellow, grey)',
};

(localStorage.getItem('Theme') === 'blueTheme') ? changeTheme(blueTheme) : changeTheme(orangeTheme);
button.addEventListener("click", function () {
 if (localStorage.getItem('Theme') === 'blueTheme') {
    changeTheme(orangeTheme);
    localStorage.setItem('Theme', 'orangeTheme');
 } else {
    changeTheme(blueTheme);
    localStorage.setItem('Theme', 'blueTheme');
 }

});

function changeTheme(item) {
  document.body.style.backgroundImage = item.background;
  document.body.style.color = item.fontColor;

  for (let i=0; i < cardDetails.length; i++) {
    cardDetails[i].style.backgroundColor = item.cardDetailsColor;
  }

  for (let i=0; i < links.length; i++) {
    links[i].style.color = item.linkColor;
  }

  for (let i=0; i < cards.length; i++) {
    cards[i].style.background = item.borderGradientColor;
  }
}
