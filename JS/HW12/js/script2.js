const image = document.querySelector('img');
const buttons = document.getElementsByTagName('button');
const timer = document.getElementsByClassName('timer')[0];
let imageCounter = 0;
let countdown;
let showTime;
let stopTimer = false;

function imgLoop(i) {
  if (i < 4) {
    i++;
    showImg (i);
  } else {
    i = 1;
    showImg (i);
  }
}

function startTime() {
  countdown.setTime(showTime - Date.now());
  timer.innerHTML = `Осталось: ${countdown.getUTCSeconds()}:${countdown.getUTCMilliseconds()}`;

  if (stopTimer) return false;

  if (countdown.getUTCSeconds() > 0) {
    requestAnimationFrame(startTime);
  }

  else {
    cancelAnimationFrame(startTime);
    imgLoop(imageCounter);
  }
}

function showImg(i) {
  image.src = `./img/${i}.jpg`;
  imageCounter = i;
  countdown = new Date();
  showTime = new Date(Date.now() + 10000);
  requestAnimationFrame(startTime);
}

buttons[0].onclick = () => {
  stopTimer = true;
};

buttons[1].onclick = () => {
  showTime = new Date(Date.now() + countdown.getUTCSeconds()*1040);
  stopTimer = false;
  requestAnimationFrame(startTime);
};

function fadeIn(timePassed) {
  image.style.opacity = `${timePassed/5}`;
}

imgLoop(imageCounter);
