const image = document.querySelector('img');
const buttons = document.getElementsByTagName('button');
const imgCount = 4;

const imgLoop = function (i) {
  image.src = `./img/${i}.jpg`;
  i++;
  if (i <= imgCount+1) {
    const showImg = setTimeout(imgLoop, 10000, i);
    buttons[0].onclick = () => clearTimeout(showImg);
    buttons[1].onclick = () => imgLoop (i-1);
  } else {
    imgLoop(1);
  }
};

imgLoop(1);
