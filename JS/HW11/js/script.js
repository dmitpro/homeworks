const buttons = document.getElementsByTagName('button');
let arrBtn = [];

for (let i=0; i < buttons.length; i++) {
  arrBtn.push(buttons[i].innerHTML.toUpperCase());
}

document.addEventListener('keyup', function (event) {
  const currentKey = event.key.toUpperCase();

  if (!arrBtn.includes(currentKey)) {
    return true
  } else {
    for (let i = 0; i < buttons.length; i++) {
      buttons[i].style.backgroundColor = '#33333a';

      if (arrBtn[i] === currentKey) buttons[i].style.backgroundColor = '#0000ff';
    }
  }
});
