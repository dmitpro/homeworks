const ul = document.querySelector('.tabs');
ul.addEventListener('click', function (event) {
  for (let item of document.querySelectorAll('.tabs-title')) {
    item.classList.remove('active');
  }
  for (let item of document.querySelectorAll('.text-content')) {
    item.classList.remove('open');
  };
    document.querySelector(`.text-content[data-tab-content='${event.target.dataset.tab}']`).classList.add('open');
    event.target.classList.add('active');
});
