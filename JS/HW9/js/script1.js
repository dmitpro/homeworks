const ul = document.querySelector('.tabs');
const tabsTitle = document.getElementsByClassName('tabs-title');
const tabContent = document.getElementsByClassName('text-content');
ul.onclick = function titleClick(event) {
  for (let item of tabsTitle) {
    item.classList.remove('active');
  }
  for (let item of tabContent) {
    item.style.display = 'none';
  }
  event.target.classList.add('active');
    tabContent[[...tabsTitle].indexOf(event.target)].style.display = 'list-item';
};