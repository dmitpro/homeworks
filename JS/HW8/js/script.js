const divPrice = document.getElementsByClassName('price')[0];
const inputPrice = document.getElementById('input-price');
const priceInfo = document.getElementsByClassName('price-info')[0];
const priceText = document.getElementsByClassName('price-text')[0];
const closeButton = document.getElementsByClassName('close-btn')[0];
const errorMessage = document.createElement('p');

function inputFocus() {
  errorMessage.remove();
  divPrice.style.backgroundColor = 'green';
  closeClick();
};

function inputBlur() {
  if (inputPrice.value) {
    if (+inputPrice.value > 0) {
      divPrice.style.backgroundColor = 'white';
      inputPrice.style.color = 'green';
      priceText.innerHTML = `Current price: $${inputPrice.value}   `;
      priceInfo.style.visibility = 'visible';
    } else {
      divPrice.style.backgroundColor = 'red';
      errorMessage.innerHTML = `Please enter correct price`;
      divPrice.after(errorMessage);
    }
  }
};

function closeClick() {
  priceInfo.style.visibility = 'hidden';
  inputPrice.value = '';
  inputPrice.style.color = 'black';
};

closeButton.addEventListener("click", closeClick);
inputPrice.addEventListener('focus', inputFocus);
inputPrice.addEventListener('blur', inputBlur);
inputPrice.addEventListener('keyup', (function(event) {
  if(event.code == "Enter") {
    inputPrice.blur();
  }
}));




