const filterBy = function (arr, type) {
  return arr.filter(item => typeof(item) !== type);
};

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
console.log(filterBy(['Toyota', NaN, 123, undefined, '', -543, false, {}, null, 0, [], 45677.45], 'number'));


