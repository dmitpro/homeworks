###	Ответы на теоретические вопросы:

1. Переменным, объявленым через var или let можно присваивать и потом
переприсваивать произвольные значения. Переменная, объявленная
через const не может принимать никакое другое значение кроме
изначально присвоенного, при объявлении через const всегда требуется инициализация. 
Объявленная через var переменная будет видна во всем тексте программы 
независимо от места ее объявления, а к переменной, объявленной через let
можно будет обращаться только после того как она будет объявлена и 
видимость ее ограничена блоком.

2. Объявлять переменную через var считается плохим
тоном из-за области видимости переменной во всем тексте программы, а также
из-за возможности повторного переобъявления переменной.