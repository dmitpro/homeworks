let name;
let age;
do {
	name = prompt('Enter you name:', name);
	if (name == null) {
		name = "";
	}
	age = prompt('How old are you?', age);
	if (age == null) {
		break;
	}
}	while (!name || !age || (isNaN(+age)));
if (age > 22) {
	alert (`Welcome, ${name}`);
} else if (age >= 18 && age <= 22) {
	if (confirm('Are you sure you want to continue?')) {
		alert(`Welcome, ${name}`);
	} else {
		alert('You are not allowed to visit this website');
	}
} else {
	alert ('You are not allowed to visit this website');
}