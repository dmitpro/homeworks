let m;
let n;
let correct = false;
while (!correct) {
	m = prompt('Enter a number 1:');
	n = prompt('Enter a number 2:');
	if (m === null || n === null) {
		break;
	} else {
		m = +m;
		n = +n;
	}
	if (	 m === 0
			|| m !== Math.floor(m)
			|| isNaN(m)
			|| n === 0
			|| n !== Math.floor(n)
			|| isNaN(n)) {
		alert("Error! Enter correct number.")
	} else {
		correct = true;
	}
}
if (m > n) {
	let k = m;
	m = n;
	n = k;
}
prime: for (let i = m; i <= n; i++) {
	for (let j = 2; j <= Math.sqrt(i); j++) {
		if (i % j === 0) continue prime;
	}
	if (i > 1) {
		console.log(i);
	}
}


