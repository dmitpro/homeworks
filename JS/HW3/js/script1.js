const x = +prompt('Enter number 1:');
const y = +prompt('Enter number 2:');
const operator = prompt('Enter mathematical operator ( + , - , * , / ):');

const calculate = function(a, b, symbol) {
  let result;
  switch (symbol) {
    case '+':
      result = a + b;
      break;
    case '-':
      result = a - b;
      break;
    case '/':
      result = a / b;
      break;
    case '*':
      result = a * b;
      break;
    default:
      alert('Enter correct sign');
      break;
  }
  return result;
};

console.log(`Result: ${x} ${operator} ${y} = ${calculate(x, y, operator)}`);

