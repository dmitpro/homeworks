let x;
let y;
let operator;
let correct;
const mathSymb = '+-*/';

const calculate = function(a, b, symbol) {
  let result;
  switch (symbol) {
    case '+':
      result = +a + +b;
      break;
    case '-':
      result = a - b;
      break;
    case '/':
      result = a / b;
      break;
    case '*':
      result = a * b;
      break;
  }
  return result;
};

while (!correct) {
  x = prompt('Enter number 1:', x);
  if (x == null) break;

  y = prompt('Enter number 2:', y);
  if (y == null) break;

  operator = prompt('Enter mathematical operator ( + , - , * , / ):', operator);
  if (operator == null) break;

  if (   isNaN(x)
      || isNaN(y)
      || !mathSymb.includes(operator)) {
    alert('Enter correct information')
  } else {
    correct = true;
  }

}

if (correct) {
  console.log(`result: ${x} ${operator} ${y} = ${calculate(x, y, operator)}`);
}
