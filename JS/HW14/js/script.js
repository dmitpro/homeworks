$('.tabs-title').click(function() {
  const currentCategory = $(this).data('category');
  $('.tabs-title').removeClass('active');
  $('.text-content').removeClass('open');
  $(`.text-content[data-category=${currentCategory}]`).addClass('open');
  $(this).addClass('active');
});