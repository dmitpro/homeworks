const arrToList = function (arr) {
  let list = document.createElement('ul');
  const listItems = arr.map(item => '<li>'+item+'</li>');
  for (let item of listItems) {
    list.innerHTML += item;
  }
  document.body.append(list);
}

arrToList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
arrToList(['1', '2', '3', 'sea', 'user', 23]);