const arrToList = function (arr) {
  let list = document.createElement('ul');
  const listItems = arr.map(item => {
    if (typeof(item) === 'object') {
      let underList = '<ul>';
      for (let uItem in item) {
        underList += '<li>' + item[uItem] + '</li>';
      }
      underList += '</ul>';
      return underList;
    } else { return '<li>'+item+'</li>';
    }
  });
  for (let item of listItems) {
    list.innerHTML += item;
  }
  document.body.append(list);
};

const clearTimer = function() {
  let sec = 9;
  let timer = setInterval(function() {
    if (sec >= 0) {
      document.getElementById("timer").innerHTML = sec.toString();
      sec--;
    }  else {
      document.body.remove();
      clearInterval(timer);
    }
  }, 1000);
};

arrToList(['Mercury', 'Venus', 'Earth', {satellite: 'Moon'}, 'Mars', ['Deimos', 'Phobos'], 'Jupiter', ['Ganymede', 'Callisto',
  'Io', 'Europa'], 'Saturn', ['Titan', 'Enceladus'], 'Uranus', ['Titania', 'Oberon', 'Umbriel', 'Ariel', 'Miranda'], 'Neptune']);
clearTimer();

