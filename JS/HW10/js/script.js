const inputPass = document.getElementsByTagName('input');

const showPass = function (element) {
  element.setAttribute('type', 'text');
};

const hiddenPass = function (element) {
  element.setAttribute('type', 'password');
};

dontMatchPass = document.createElement('p');
dontMatchPass.style.color = 'red';
dontMatchPass.innerHTML = "Нужно ввести одинаковые значения";
document.forms[0].addEventListener('click', function (event) {

  if (event.target.tagName == "I") {
    event.target.classList.toggle("fa-eye-slash");
    event.target.classList.toggle("fa-eye");

    if (event.target.className == "fas icon-password fa-eye") {
      showPass(event.target.previousElementSibling);
    } else if (event.target.className == "fas icon-password fa-eye-slash") {
      hiddenPass(event.target.previousElementSibling);
    }
  };

  if (event.target.tagName == "BUTTON") {
    event.preventDefault();
    if (inputPass[0].value !== '' && inputPass[0].value === inputPass[1].value) {
      alert('You are welcome');
      inputPass[0].value = inputPass[1].value = '';
    } else {
      event.target.before(dontMatchPass);
    }
  };

  if(event.target.tagName == "INPUT") {
    dontMatchPass.remove();
    hiddenPass(event.target.nextElementSibling);
  };

});
