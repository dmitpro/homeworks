const modalStartParam = {
  modalTitle: `<h3>NEW GAME</h3><p>Select difficulty:</p>`,
  modalContent: `<form id="formNewGame">
                  <p><input type="radio" name="difficulty" value=1500> easy</p>
                  <p><input type="radio" name="difficulty" value=1000 checked> middle</p> 
                  <p><input type="radio" name="difficulty" value=500> hard</p>
                  <input type="submit" name="submit-btn" class="button" value="Start">
                  <input type="reset" name="reset-btn" class="button" value="Cancel">
                </form>`
};

class Modal {
  constructor(obj) {
    this.modalTitle = obj.modalTitle;
    this.modalContent = obj.modalContent;
    this.elem = null;
  }

  render() {
    this.elem = document.createElement('div');
    this.elem.classList.add('modal');
    this.elem.insertAdjacentHTML('beforeend', this.modalTitle);
    this.elem.insertAdjacentHTML('beforeend', this.modalContent);
    this.elem.addEventListener('submit', this.submitModal.bind(this));
    this.elem.querySelector('[type="reset"]').onclick = function () {
      document.querySelector('.modal').remove();
      container.innerHTML = '';
      root.insertAdjacentHTML('beforeend', `<p class='modal'>Bye!</p>`);
    };
    return this.elem;
  }

  submitModal(e) {
    e.preventDefault();

    if (e.target.closest('#formNewGame')) {
      const level = +document.querySelector('input[name="difficulty"]:checked').value;
      this.elem.remove();
      const game1 = new Game ('', '', level);
      container.append(game1.render());
      game1.startGame();
    }

    if (e.target.closest('#formWinner')) {
      container.innerHTML = '';
      this.elem.remove();
      root.append(newGameModal.render());
    }
  }
}

class Game {
  constructor(sizeH, sizeV, level) {
    this.sizeH = sizeH || 10;
    this.sizeV = sizeV || 10;
    this.level = level || 1000;
    this.maxScore = this.sizeH * this.sizeV / 2;
    this.computerScore = 0;
    this.userScore = 0;
    this.index = null;
    this.colorizeDelay = null;
    this.userDelay = null;
    this.loop = null;
    this.td = null;
    this.elem = null;
  }

  render() {
    container.insertAdjacentHTML('beforeend',
        `<h2>You score: <span class="user-score"></span></h2>
               <h2>Computer score: <span class="computer-score"></span></h2>`);
    this.elem = document.createElement('table');
    for (let i = 1; i <= this.sizeH; i++) {
      const tr = document.createElement('tr');
      this.elem.append(tr);
      for (let j = 1; j <= this.sizeV; j++) {
        const td = document.createElement('td');
        td.classList.add('cell-grey');
        tr.append(td);
      }
    }
    this.elem.addEventListener('click', this.clickOnCell.bind(this));
    return this.elem;
  }

  clickOnCell(e) {

    if (e.target.classList.contains('cell-blue')) {
      clearTimeout(this.userDelay);
      e.target.classList.remove('cell-blue');
      e.target.classList.add('cell-green');
      this.userScore++;
      document.querySelector('.user-score').innerHTML = `${this.userScore}`;
    }
  }


  startGame() {
    this.colorizeDelay = setTimeout(this.activateCell.bind(this), this.level);
  }


  activateCell() {
    this.td = this.elem.querySelectorAll('.cell-grey');

    if (this.computerScore < this.maxScore && this.userScore < this.maxScore) {
      this.index = generateRandom(this.td.length);
      this.td[this.index].setAttribute('class', 'cell-blue');
        this.userDelay = setTimeout(this.paintRed.bind(this), this.level);
    } else {
      clearTimeout(this.loop);
      clearTimeout(this.colorizeDelay);
      const winner = (this.computerScore > this.userScore) ? 'Computer' : 'You';
      const modalWinnerParam = {
        modalTitle: `<h3>Game Over</h3>`,
        modalContent: `<h2>${winner} winner!</h2>
                       <p>Play again?</p>
                       <form id="formWinner">
                        <input type="submit" name="submit-btn" class="button" value="Yes">
                        <input type="reset" name="reset-btn" class="button" value="No">
                       </form>`
      };
      const winnerModal = new Modal(modalWinnerParam);
      root.append(winnerModal.render());
      return;
    }
    this.loop = setTimeout(this.activateCell.bind(this), this.level)
  }


  paintRed() {

    if (!this.td[this.index].classList.contains('cell-green')) {
      this.td[this.index].classList.remove('cell-blue');
      this.td[this.index].classList.add('cell-red');
      this.computerScore++;
      document.querySelector('.computer-score').innerHTML = `${this.computerScore}`;
    }
  }
}

const root = document.querySelector('#root');
const container = document.getElementById('container');
const newGameModal = new Modal(modalStartParam);
root.append(newGameModal.render());


function generateRandom(len) {
  return Math.floor( Math.random() * len);
}
