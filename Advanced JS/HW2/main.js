class Hamburger {
  constructor (size, stuffing) {
    try {

      if (!size) {
        throw new HamburgerException('no size given');
      }
      if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
        throw new HamburgerException('invalid size');
      }
      this.size = size;
    } catch (error) {
        console.log(error);
    }

    try {

      if (!stuffing) {
        throw new HamburgerException('no stuffing given');
      }
      if (stuffing !== Hamburger.STUFFING_CHEESE
          && stuffing !== Hamburger.STUFFING_SALAD
          && stuffing !== Hamburger.STUFFING_POTATO) {
        throw new HamburgerException('add correct stuffing');
      }
      this.stuffing = stuffing;
    } catch (error) {
      console.log(error);
    }

    this.topping = [];
  }

  addTopping(topping) {
    try {

      if(!topping) {
        throw new HamburgerException('no topping given');
      }
      if (topping !== Hamburger.TOPPING_MAYO
          && topping !== Hamburger.TOPPING_SPICE) {
        throw new HamburgerException('invalid topping name');
      }
      if (this.topping.includes(topping)) {
        throw new HamburgerException('duplicate topping');
      }
      this.topping.push(topping);
      return true;
    } catch (error) {
      console.log(error);
    }
  };

  removeTopping(topping) {
    const indexOfTopping = this.topping.indexOf(topping);
    if (indexOfTopping !== -1) {
      this.topping.splice(indexOfTopping, 1);
    }
  };

  getToppings() {
    return this.topping;
  };

  getSize() {
    return this.size;
  };

  getStuffing() {
    return this.stuffing;
  };

  calculatePrice() {
    let toppingPrice = 0;
    this.topping.forEach((topping) => toppingPrice += topping.price);
    return this.size.price + this.stuffing.price + toppingPrice;
  };

  calculateCalories() {
    let toppingCalories = 0;
    this.topping.forEach((topping) => toppingCalories += topping.calories);
    return this.size.calories + this.stuffing.calories + toppingCalories;
  }
}

Hamburger.SIZE_SMALL = {
  price: 50,
  calories: 20
};

Hamburger.SIZE_LARGE = {
  price: 100,
  calories: 40
};

Hamburger.STUFFING_CHEESE = {
  price: 10,
  calories: 20
};

Hamburger.STUFFING_SALAD = {
  price: 20,
  calories: 5
};

Hamburger.STUFFING_POTATO = {
  price: 15,
  calories: 10
};

Hamburger.TOPPING_SPICE = {
  price: 15,
  calories: 0
};

Hamburger.TOPPING_MAYO = {
  price: 20,
  calories: 5
};

function HamburgerException(property) {
  this.message = "Error: " + property
}

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
console.log("Have %d toppings", hamburger.getToppings().length);
