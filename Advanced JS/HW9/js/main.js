window.onload = function () {
  document.querySelector('.contact-btn').addEventListener('click', getRequest);

  async function getRequest() {
    document.cookie = 'experiment=novalue; Max-Age=300';
    document.cookie = (document.cookie.includes('new-user')) ? 'new-user=false' : 'new-user=true';
  }
};