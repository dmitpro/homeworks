document.getElementById('search-btn').addEventListener('click', getRequest);

  async function getRequest() {
    const requestIP = await fetch('https://api.ipify.org/?format=json');
    const {ip} = await requestIP.json();
    const requestPosition = await fetch(`http://ip-api.com/json/${ip}?fields=1572889&lang=ru`);
    const {continent, country, regionName, city, district} = await requestPosition.json();
    const positionInfo = `<ul><li>Континент: ${continent}</li><li>Страна: ${country}</li>
          <li>Регион: ${regionName}</li><li>Город: ${city}</li><li>Район города: ${district}</li>`;
    document.getElementById('container').innerHTML = positionInfo;
  };
