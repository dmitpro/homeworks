function getRequest (url, func, ...arg) {
  const xhr = new XMLHttpRequest();
  xhr.open('GET', url);
  xhr.responseType = 'json';
  xhr.send();
  xhr.onload = function() {
    if (xhr.status >= 300) {
      console.log(`Error ${xhr.status}: ${xhr.statusText}`);
    } else {
      const {response} = xhr;
      func(response, ...arg);
    }
  }
}

function createSwItem(obj, selector) {
  const swList = sortElem(obj.results, 'episode_id');

  swList.forEach(item => {
    const {episode_id, title, opening_crawl, characters} = item;
    const swItem = `<div class ="sw-item"><h3>Episode ${item.episode_id}</h3><h2>${title}</h2>
                    <p style="color: grey">Characters:</p><ul id="char${episode_id}" class="characters"></ul>
                    ${spinner}<p class="description">${opening_crawl}</p><hr></div>`;
    document.querySelector(selector).insertAdjacentHTML('beforeend', swItem);

    characters.forEach(link => getRequest(link, createCharacters, document.querySelector(`#char${episode_id}`)));

    setTimeout(function() {
      const charactersContainer = document.getElementsByClassName('characters');
      for (const elem of charactersContainer) {
        elem.style.display = 'block';
        elem.nextElementSibling.style.display = 'none';
      }
    }, 4000)
  })
}

function createCharacters(object, container) {
  container.insertAdjacentHTML('beforeend', `<li>${object.name}</li>`);
}

function sortElem(arr, value) {
  arr.sort(function (a, b) {
    if (a[value] > b[value]) return 1;
    if (a[value] < b[value]) return -1;
  });
  return arr;
}

const spinner = '<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div>' +
    '<div></div><div></div><div></div><div></div><div></div><div></div></div>';

getRequest('https://swapi.dev/api/films/', createSwItem, '#container');