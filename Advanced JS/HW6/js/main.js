import {icons, btnEdit, btnDel}  from '../js/variables.js';

const users = [];

function getRequest(url, method, headers, reqData, func, ...arg) {
  $.ajax({
    url: url,
    method: method,
    headers: headers,
    data: reqData,
    dataType: 'json',
    success: function (data) {

      if (Array.isArray(data)){
        $.each(data, item => {
          func(data, item);
        })
      } else func(data, ...arg)
    },
    error: showError
  });
}

function showError(err){
  alert(err);
}

function renderPost(postsData, postItem= '') {
  $("#spinner").removeClass("show");
  const {userId, id, title, body} = (typeof(postItem) == 'number') ? postsData[postItem] : postsData;
  const currentUser = users.find(item => item.id === userId);
  const {name, email} = currentUser;
  const userNameArr = name.split(' ');
  const userLogo = userNameArr[0].slice(0,1) + userNameArr[1].slice(0,1);
  const cardHtml = `<div id=${id} class="card">
                      <div class="user-logo-container"><h3 class="user-logo">${userLogo}</h3></div>
                      <div class="card-container">
                        <a href="#" class="alert-link">${name}</a>
                        <span class="user-email">  ${email}</span>${btnEdit} ${btnDel}
                        <h4 class="card-title">${title}</h4>
                        <p class="card-text">${body}</p>
                        <div class="icons">${icons}</div>
                      </div>
                    </div>`;
  $("#container").prepend(cardHtml);
}

function createUsersList (usersData, usersItem) {
  users.push(usersData[usersItem]);
}

document.addEventListener('click', (e) => {

  if (e.target.closest(".btn-del")) {
    e.preventDefault();
    if (confirm('Delete post?')) {
      const delId = +e.target.closest(".card").id;
      $("#spinner").addClass("show");
      getRequest(`https://jsonplaceholder.typicode.com/users/${delId}`,
          'DELETE', '','', delPost, delId);
    }
  }

  if (e.target.closest(".btn-edit")) {
    const formEdit = $(".edit-post-wrapper");
    formEdit.addClass("show");
    $('[type="reset"]').click(() => formEdit.removeClass("show"));
    const editId = +e.target.closest(".card").id;
    $("[name='edit-title']").val($(`#${editId}`).find(".card-title").text());
    $("[name='edit-text']").val($(`#${editId}`).find(".card-text").text());
    $("#form-edit-post").submit(function (e) {
      e.preventDefault();
      const editData = {
        id: editId,
        title: $("[name='edit-title']").val(),
        body: $("[name='edit-text']").val(),
        userId: 1,
      };
      formEdit.removeClass("show");
      $("#spinner").addClass("show");
      getRequest(`https://jsonplaceholder.typicode.com/posts/${editId}`, 'PUT',
          {"Content-type": "application/json; charset=UTF-8"}, JSON.stringify(editData), editPost);
    });
  }

  if (e.target.closest(".btn-add")) {
    addPost();
  }
});

function delPost(delData, idx) {
  $("#spinner").removeClass("show");
  $(`#${idx}`).remove();
}

function editPost(editedData) {
  $("#spinner").removeClass("show");
  const {body, title, id} = editedData;
  const postId = $(`#${id}`);
  postId.find(".card-title").text(title);
  postId.find(".card-text").text(body);
}

function addPost() {
  const formAdd = $(".add-post-wrapper");
  formAdd.addClass("show");
  $('[type="reset"]').click(() => formAdd.removeClass("show"));
  $("#form-add-post").submit(function (e) {
    e.preventDefault();
    const actionForm = this.getAttribute('action');
    const postData = {
      title: $("[name='add-title']").val(),
      body: $("[name='add-text']").val(),
      userId: 1,
    };
    formAdd.removeClass("show");
    $("#spinner").addClass("show");
    getRequest(actionForm, 'POST',{"Content-type": "application/json; charset=UTF-8"},
        JSON.stringify(postData), renderPost);
  });
}

getRequest('https://jsonplaceholder.typicode.com/users', 'GET', '', '', createUsersList);

setTimeout(function () {
  getRequest('https://jsonplaceholder.typicode.com/posts', 'GET', '','', renderPost);
}, 200);
