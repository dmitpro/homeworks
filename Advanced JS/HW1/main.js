function Hamburger (size, stuffing) {
  try {

    if (!size) {
      throw new HamburgerException('no size given');
    }
    if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
      throw new HamburgerException('invalid size');
    }
    this.size = size;
  } catch (error) {

    if (error instanceof HamburgerException) {
      alert(error.message);
      return false;
    } else {
      throw error;
    }
  }

  try {

    if (!stuffing) {
      throw new HamburgerException('no stuffing given');
    }
    if (stuffing !== Hamburger.STUFFING_CHEESE
        && stuffing !== Hamburger.STUFFING_SALAD
        && stuffing !== Hamburger.STUFFING_POTATO) {
      throw new HamburgerException('add correct stuffing');
    }
    this.stuffing = stuffing;
  } catch (error) {

    if (error instanceof HamburgerException) {
      alert(error.message);
      return false;
    } else {
      throw error;
    }
  }

  this.topping = [];
}

Hamburger.SIZE_SMALL = {
  price: 50,
  calories: 20
};

Hamburger.SIZE_LARGE = {
  price: 100,
  calories: 40
};

Hamburger.STUFFING_CHEESE = {
  price: 10,
  calories: 20
};

Hamburger.STUFFING_SALAD = {
  price: 20,
  calories: 5
};

Hamburger.STUFFING_POTATO = {
  price: 15,
  calories: 10
};

Hamburger.TOPPING_SPICE = {
  price: 15,
  calories: 0
};

Hamburger.TOPPING_MAYO = {
  price: 20,
  calories: 5
};

Hamburger.prototype.addTopping = function (topping) {
  try {

    if(!topping) {
      throw new HamburgerException('no topping given');
    }
    if (topping !== Hamburger.TOPPING_MAYO
        && topping !== Hamburger.TOPPING_SPICE) {
      throw new HamburgerException('invalid topping name');
    }
    if (this.topping.includes(topping)) {
      throw new HamburgerException('duplicate topping');
    }
    this.topping.push(topping);
    return true;
  } catch (error) {

    if (error instanceof HamburgerException) {
      alert(error.message);
      return false;
    } else {
      throw error;
    }
  }
};

Hamburger.prototype.removeTopping = function (topping) {
  var indexOfTopping = this.topping.indexOf(topping);
  if (indexOfTopping !== -1) {
    this.topping.splice(indexOfTopping, 1);
  }
};

Hamburger.prototype.getToppings = function () {
  return this.topping;
};

Hamburger.prototype.getSize = function () {
  return this.size;
};

Hamburger.prototype.getStuffing = function () {
  return this.stuffing;
};

Hamburger.prototype.calculatePrice = function () {
  var toppingPrice = 0;
  for (var idx in this.topping) {
    toppingPrice += this.topping[idx].price;
  }
  return this.size.price + this.stuffing.price + toppingPrice;
};

Hamburger.prototype.calculateCalories = function () {
  var toppingCalories = 0;
  for (var idx in this.topping) {
    toppingCalories += this.topping[idx].calories;
  }
  return this.size.calories + this.stuffing.calories + toppingCalories;
};

function HamburgerException (property) {
  this.message = "Error: " + property
}

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
console.log("Have %d toppings", hamburger.getToppings().length);
